## What is this repository for?
###This repo contains files regarding the testdata for Ghost Inspector and Apica load tests.

## Links:
* Ghost Inspector  
⋅⋅* [Ghost Inspector Wiki](https://confluence.istone.se/display/xxl/Ghost+Inspector)  
⋅⋅* [Ghost Inspector](https://ghostinspector.com/)

*How to: Initiate a Loadtest - Apica  
⋅⋅* [Apica Wiki](https://confluence.istone.se/pages/viewpage.action?pageId=29896520)

## Things to do after a database reset:  
### Import Impex files in backoffice from `/Product_Impex/`
  
* Register new user with the registerAccounts_GI_APICA.impex)    
* setProductInStock-LOAD.impex 
* setProductsInStock-GHOST.impex
* registerAccounts_GI_APICA.impex
* promotionVoucher_GI.impex

### Who do I talk to?  
* Repo owner : Dennis Kristensson
* Team contact Victor Bjerre
